<?php
/*
Plugin Name: Ice Booking
Plugin URI:  
Description: <p>This is plugins is <strong>ONGOING CONSTRUCTION...</strong><br> The main goal for this plugins is to build an admin dashboard wherein administrator can monitor the list for rooms available, type of room, price changes, and quick reservation for a hotel.</p>
Version: <strong>1.0</strong> 
Author: <strong>Joseph Eguna</strong><br> Email: <a href="mailto:joseph.eguna@gmail.com"><strong>joseph.eguna@gmail.com</strong></a>
*/

/*
 */
require_once( 'inc/ice_booking.php');  
require_once( 'inc/ice_booking_menu.php');  
require_once( 'inc/ice_booking_filter.php');  
require_once( 'inc/ice_booking_calendar.php');
require_once( 'inc/ice_booking_options.php');
#require_once( 'inc/ice_shortcode.php');  

register_activation_hook( __FILE__, array('icebooking_class', 'create_ice_booking_tables') );
add_action( 'admin_menu', array('icebooking_class_menu', 'create_ice_booking_menu') );
add_action( 'admin_init', array('icebooking_class_menu', 'load_style') );
add_action( 'admin_enqueue_scripts', array('icebooking_class_menu', 'load_script') );
add_action( 'wp_ajax_jaction', 		 array('icebooking_class_options', 'process_ajax_form') );


