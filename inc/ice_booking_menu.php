<?php 
#Ice Booking Menu Class
if(!class_exists('icebooking_class_menu')):
class icebooking_class_menu 
{
	#CREATE ADMIN MENU
	public function create_ice_booking_menu()
	{
		add_menu_page( 'Ice Booking Option', 
					   'Ice Booking', 
					   'manage_options', 
					   'ice-booking', 
					   array('icebooking_class_options','list_room_framework'), '', 6 ); 
		
		add_submenu_page( 'ice-booking',
						  'Ice Calendar',
						  'Ice Calendar',
						  'manage_options',
						  'ice-calendar',
						  array('icebooking_class_options', 'ice_calendar') );
						  
		add_submenu_page( 'ice-booking',
						  'Add Room Type',
						  'Add Room Type',
						  'manage_options',
						  'add-room-type',
						  array('icebooking_class_options', 'add_roomtype') );
						  
		add_submenu_page( 'ice-booking',
						  'Add Room Specs',
						  'Add Room Specs',
						  'manage_options',
						  'add-rooms',
						  array('icebooking_class_options', 'add_room') );
		
		add_submenu_page( 'ice-booking',
						  'Add Constants',
						  'Add Constants',
						  'manage_options',
						  'add-rooms-constants',
						  array('icebooking_class_options', 'add_room_constants') );
	}
	
	#ENQUEUE STYLE
	public function load_style(){
		
		if(!$_GET['page'] == 'ice-booking')
			return false;
	 $style = array( 'ice-style' 		  => 'admin/style.css',
					 'ice-style-calendar' => 'admin/fullcalendar.css',
					 'ice-style-ui'       => 'admin/jquery-ui.css',
					 'ice-style-res'      => 'admin/style-responsive.css',
					 'thickbox'			  => false
					 );
					 
		if(!empty($style)):
		foreach($style as $kstyle => $vstyle):
			if(!$vstyle):
				wp_enqueue_style($kstyle);
			else:
		wp_enqueue_style( $kstyle, 
						   plugins_url('ice-booking') . '/css/'.$vstyle, 
						   array() );
			endif;
		endforeach;
		endif;
	}
	
	#ENQUEUE SCRIPT
	public function load_script(){
		
		/*return;
		if(!$_GET['page'] == 'ice-booking')
			return false;
		
		/*$js = array( 'jquerymin'	=> 'jquery/jquery-1.8.2.min.js',
					 'jqueryminui' 	=> 'jquery/jquery-ui.min.js',
					 'bootstrapmin' => 'bootstrap/bootstrap.min.js',
					 'slimscroll' 	=> 'jquery/jquery.slimscroll.min.js',
					 'jscookie' 	=> 'jquery.cookie.js',
					 'jquerytables' => 'tables/jquery.dataTables.js',
					 'tables' 		=> 'tables/data-table.js'
					 );*/
		
		$js = array( 'jquery'		=> false,
					 'thickbox' 	=> false,
					 'ui-cal' 		=> 'jquery-ui.custom.min.js',
					 'calendar'     => 'fullcalendar.js',
					 'custom' 		=> 'custom.js' 
					 );
		if(!empty($js)):
		foreach($js as $kjs => $vjs):
			if(!$vjs):
				wp_enqueue_script($kjs);
			else:
				wp_enqueue_script( $kjs, 
							   plugins_url('ice-booking') . '/js/'.$vjs, 
							   array(),false, true );
			endif;
		endforeach;
		endif;
	}
	
}
endif;
