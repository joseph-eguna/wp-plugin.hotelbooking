<?php 
#Ice Booking Class Options
if(!class_exists('icebooking_class_options')):
class icebooking_class_options 
{
	public function __construct()
	{
		#$this->options_framework();
		return;
	}
	
	public function options_framework()
	{
?>
		
<?php
	}
	
	#FUNCTION ADD ROOM TYPE
	function add_roomtype()
	{
		global $wpdb;
		$input = $_REQUEST;
		$sql = "SELECT * FROM {$wpdb->prefix}ice_amenities;";
		$amenities = $wpdb->get_results($sql);
		$title = 'Add';
		
		$sql = "SELECT * FROM {$wpdb->prefix}ice_conditions;";
		$conditions = $wpdb->get_results($sql);
		
		$id = intval($_GET['id']);
		if($_GET['action'] == 'update' AND is_int($id)):
			$roomtype_desc = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_roomtype WHERE id={$id};");
			$roomtype_cond = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_roomtypeconditions WHERE roomTypeId={$id};");
			$roomtype_amen = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_roomtypeamenities WHERE roomTypeId={$id};");
			$roomtype_medi = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_roomtypemedia WHERE roomTypeId={$id};");
			
			$input['room_type']   = $roomtype_desc[0]->roomTypeName;
			$input['description'] = $roomtype_desc[0]->description;
			$input['bed_room']    = $roomtype_desc[0]->bedRoom;
			$input['bathroom']    = $roomtype_desc[0]->bathRoom;
			
			foreach($roomtype_amen as $amen):
				$input['amenities_id'][] = $amen->amenentiesId;
			endforeach;
			
			/*foreach($roomtype_cond as $cond):
				$input['conditions_id'][] = $cond->conditionId;
			endforeach;*/
			
			$input['images'] 		= unserialize($roomtype_medi[0]->image);
			$title = 'Update';
		endif;
		
	if ( wp_verify_nonce( $_POST['add_roomtype_field'], 'add_roomtype_action' ) ):
		
		$required = array('room_type', 'description', 'amenities'=>array());
		$error = icebooking_class_filter::filter_form($_POST, array('required'=>$required)); 
		
		$data  = array('room_type'  =>$_POST['room_type'],
					   'description'=>$_POST['description'],
					   'bed_size'   =>$_POST['bed_size'],
					   'bathroom'   =>$_POST['bathroom'],
					   'amenities'  =>$_POST['amenities'],
					   'conditions' =>$_POST['conditions']);
		$data['media'] =  array('image'=> $_POST['image'],
								'video' =>'Chttp://video.com/video1.swf');
		
		if(empty($error)):
			if($_GET['action'] == 'update'):
				$result = icebooking_class::update_roomtype($data, $id);
			else:
				$result = icebooking_class::add_roomtype($data);
			endif;
		endif; 
	endif;
?>
<div class="ice_booking one_half">
<?php 
	if($_GET['action'] == 'delete'):
		$result = icebooking_class::delete_roomtype($id);
		icebooking_class_filter::notification(0, array('Deleted!')); 
		?>
	<script type="text/javascript">
		setTimeout(function () { window.location = "<?php echo admin_url('admin.php?page=ice-booking'); ?>"; }, 1000);
	</script>
		<?php
		return;
	endif;
	
	if ( wp_verify_nonce( $_POST['add_roomtype_field'], 'add_roomtype_action' ) ):
		icebooking_class_filter::notification($result, $error); 
	endif; 
?>
<h1>Add Room Type</h1>
<form id="add_room_type" class="form_submit" enctype="multipart/form-data" method="POST" action="<?php #echo admin_url('admin.php?page=ice-booking'.$url) ?>">
<table class="list">
<thead>
	<tr>
		<td width="120px">Label</td>
		<td>Value</td>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Room type: </td>
		<td><input type="text" name="room_type" value="<?php echo str_replace('\\', '',$input['room_type']); ?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Room Description:</td>
		<td>
			<textarea name="description" class="textfield"><?php echo str_replace('\\', '',$input['description']); ?></textarea>
		</td>
	</tr>
	<!--tr>
		<td>Room Size: </td>
		<td><input type="text" name="room_size" value="<?php echo str_replace('\\', '',$input['room_size']); ?>" class="textfield" /></td>
	</tr-->
	<tr>
		<td>Bed Size(s): </td>
		<td>
			<select name="bed_size" style="height:50px" class="textfield">
				<option>1 twin</option>
				<option>2 queen</option>
				<option>3 twin</option>
				<option>1 queen</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Bathrooms: </td>
		<td>
			<select name="bathroom" style="height:50px" class="textfield">
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
				<option>5</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Images:</td>
		<td class="gallery images">
			<?php if(!empty($input['images'])):?>
				<?php foreach($input['images'] as $image):?>
			<span class="new_images">
				<img width="50px" src="<?php echo $image; ?>" />
				<input type="hidden" value="<?php echo $image; ?>" name="image[]">
				<a class="remove_image"> X </a>
			</span>
				<?php endforeach;?>
				<?php #?>
			<?php endif;?>
			
			<button class="select_images button">Select Images</button>
		</td>
	</tr>
	<!--tr>
		<td>Videos:</td>
		<td><input type="text" name="videos" value="<?php echo $input['videos']?>" class="textfield" /></td>
	</tr-->
	<tr>
		<td>Amenities:</td>
		<td>
		<?php if(!empty($amenities)):?>
			<?php foreach($amenities as $v_amen): ?>
			<span class="block">
				<?php 
					if(!empty($input['amenities_id']))
						$checked = (in_array($v_amen->id, $input['amenities_id'])) ? 'checked' : ''; 
				?>
				<input type="checkbox" name="amenities[]" value="<?php echo $v_amen->id;?>" <?php echo $checked; ?>>
				<img src="<?php echo $v_amen->icon;?>" alt="<?php echo $v_amen->name;?>" title="<?php echo $v_amen->name;?>" width="25px" />
			</span>
			<?php endforeach; ?>
		<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
	<?php wp_nonce_field('add_roomtype_action','add_roomtype_field'); ?>
	<input type="submit" class="textfield submit" value="<?php echo $title;?> Room Type" />
		</td>
	</tr>
</tbody>
</table>
</form>	
</div>
<?php 	
	}
	
	#Function List of room framework
	public function list_room_framework()
	{
		global $wpdb;
		
		$list_of_roomtype = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_roomtype");
		
?>
<div class="ice_booking">
<h1>List Of Rooms</h1>
<a class="button" href="<?php echo admin_url("admin.php?page=add-room-type");?>">Add Room Type</a><br /><br />
<table class="list">
	<thead>
	<tr>
		<td width="20px"></td>
		<td width="150px">Room Type</td>
		<td>Description</td>
		<td width="100px">Amenities</td>
		<!--td width="100px">Conditions</td-->
		<td width="20px"></td>
	</tr>
	</thead>
	<tbody>
	<?php if(!empty($list_of_roomtype)):  
			foreach($list_of_roomtype as $k_roomtype => $v_roomtype): 
				$roomtype_amenid = $wpdb->get_results("SELECT * FROM  {$wpdb->prefix}ice_roomtypeamenities WHERE roomTypeId={$v_roomtype->id}");
				$rooms_lists 	 = $wpdb->get_results("SELECT * FROM  {$wpdb->prefix}ice_rooms WHERE roomTypeId={$v_roomtype->id}");
				#$roomtype_condid = $wpdb->get_results("SELECT * FROM  {$wpdb->prefix}ice_roomtypeconditions WHERE roomTypeId={$v_roomtype->id}");
	?>
	<tr>
		<td>
			<a href="<?php echo admin_url("admin.php?page=add-room-type&action=delete&id={$v_roomtype->id}");?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/delete.png'; ?>" alt="delete" style="width:20px;" />
			</a>
		</td>
		<td><?php echo str_replace('\\', '',$v_roomtype->roomTypeName); ?></td>
		<td><?php echo str_replace('\\', '',$v_roomtype->description); ?></td>
		<!--td>
			<?php if(intval($v_room->totalChilds)):?>
				<?php for($tC=0;$tC<=$v_room->totalChilds;$tC++):?>
					<img src="<?php echo plugins_url('ice-booking').'/images/icons/human.png'; ?>" width="13px" />
				<?php endfor; ?>
			<?php endif; ?>
		</td-->
		<td>
			<?php if(!empty($roomtype_amenid)):?>
			<?php foreach($roomtype_amenid as $amenities): ?>
				<?php $room_amenities = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ice_amenities WHERE id={$amenities->amenentiesId}"); ?>
				<img src="<?php echo $room_amenities->icon; ?>" alt="<?php echo $room_amenities->name; ?>" title="<?php echo $room_amenities->name; ?>" style="width:25px;" />
			<?php endforeach;  ?>
			<?php endif; ?>
		</td>
		<!--td>
			<?php if(!empty($roomtype_condid)):?>
			<?php foreach($roomtype_condid as $conditions): ?>
				<?php $room_conditions = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ice_conditions WHERE id={$conditions->conditionId}"); ?>
				<?php echo $room_conditions->conditionName.'<br />'; ?>
			<?php endforeach;  ?>
			<?php endif; ?>
		</td-->
		<td>
			<a href="<?php echo admin_url("admin.php?page=add-room-type&action=update&id={$v_roomtype->id}"); ?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/edit.png'; ?>" alt="edit" style="width:20px;" />
			</a>
		</td>
	</tr>
	<tr>
	<!-- Rooms per room type -->
	<td colspan="2"><a href="<?php echo admin_url("admin.php?page=add-rooms&id={$v_roomtype->id}"); ?>" class="button">Add Room Specs</a></td>
	<td colspan="4">
		<table class="list rooms">
		<thead>
        <tr>
            <td></td>
            <td>Condition</td>
            <td>Capacity</td>
			<td>Total Child</td>
			<td>Booking Price</td>
			<td>Price</td>
			<td></td>
        </tr>
		</thead>
		
		<?php if(!empty($rooms_lists)):?>
		<tbody>
		<?php foreach($rooms_lists as $rooms): ?>
			<?php $conditions_list = $wpdb->get_results("SELECT * FROM  {$wpdb->prefix}ice_roomtypeconditions WHERE roomId={$rooms->id}"); ?>
        <tr>
			<td>
				<a href="<?php echo admin_url("admin.php?page=add-rooms&action=delete&id={$rooms->id}");?>">
					<img src="<?php echo plugins_url('ice-booking').'/images/icons/delete.png'; ?>" alt="delete" style="width:20px;" />
				</a>
			</td>
            <td>
			<?php if(!empty($conditions_list)):?>
				<ul>
				<?php foreach($conditions_list as $conditions):?>
					<?php $condition = $wpdb->get_results("SELECT * FROM  {$wpdb->prefix}ice_conditions WHERE id={$conditions->conditionId}"); ?>	
					<li><?php echo $condition[0]->conditionName; ?></li>	
				<?php endforeach; ?>
				</ul>
			<?php endif;?>
			</td>
            <td><?php echo $rooms->capacityId; ?></td>
            <td><?php echo $rooms->totalChilds;?></td>
			<td><?php echo $rooms->regularPrice;?></td>
			<td><?php echo $rooms->bookingPrice;?></td>
			<td>
				<a href="<?php echo admin_url("admin.php?page=add-rooms&action=update&id={$rooms->id}"); ?>">
					<img src="<?php echo plugins_url('ice-booking').'/images/icons/edit.png'; ?>" alt="edit" style="width:20px;" />
				</a>
			</td>
        </tr>
		<?php endforeach; ?>
		</tbody>
		<?php endif; ?>
		
		</table>
	</td>
	</tr>
	<!-- End Rooms -->
	<?php endforeach; ?>
	</tbody>
	<?php else: ?>
	<tbody>
	<tr>
		<td colspan="9"> No rooms Inserted! </td>
	</tr>
	</tbody>
	<?php endif; ?>
</table>
</div>
<?php
	}
	
	#Function Add form framework
	public function add_room($update = false)
	{
		global $wpdb;
		$sql = "SELECT * FROM {$wpdb->prefix}ice_conditions;";
		$conditions = $wpdb->get_results($sql);
		$input = $_REQUEST;
		$title = 'Add';
		$id    = intval($_GET['id']);
		
		$roomtypes = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_roomtype;");
		
		if($_GET['action'] == 'update' AND is_int($id)):
			
			$room_desc = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_rooms WHERE id={$id};");
			$room_cond = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_roomtypeconditions WHERE roomId={$id};");
			
			$input['room_type']   		= $room_desc[0]->roomTypeId;
			$input['number_of_childs'] 	= $room_desc[0]->totalChilds;
			$input['capacity_number']   = $room_desc[0]->capacityId;
			$input['number_of_rooms']   = $room_desc[0]->roomNr;
			$input['booking_price']    	= $room_desc[0]->bookingPrice;
			$input['regular_price']    	= $room_desc[0]->regularPrice;
			
			foreach($room_cond as $cond):
				$input['conditions_id'][] = $cond->conditionId;
			endforeach;
			
			$title = 'Update';
		endif;
		
	if ( wp_verify_nonce( $_POST['add_room_field'], 'add_room_action' ) ):
		$data = array();
				
		$required = array('room_type', 'capacity_number', 'number_of_childs', 
						  'number_of_rooms','booking_price','regular_price','conditions'=>array());
		
		$error = icebooking_class_filter::filter_form($_POST, array('required'=>$required)); 
		
		$data  = array('room_type'			=>$_POST['room_type'],
					   'capacity_number'	=>$_POST['capacity_number'],
					   'number_of_childs'	=>$_POST['number_of_childs'],
					   'number_of_rooms'	=>$_POST['number_of_rooms'],
					   'booking_price'	=>$_POST['booking_price'],
					   'regular_price'	=>$_POST['regular_price'],
					   'conditions'		=>$_POST['conditions']);
					   
		if(empty($error)):
			if($_GET['action'] == 'update'):
				$result = icebooking_class::update_room($id, $data);
			else:
				$result = icebooking_class::add_room($data);
			endif;
		endif;
	endif;
?>

<div class="ice_booking">

<?php 
	if($_GET['action'] == 'delete'):
		$result = icebooking_class::delete_room($id);
		icebooking_class_filter::notification(0, array('Deleted!')); 
		?>
	<script type="text/javascript">
		setTimeout(function () { window.location = "<?php echo admin_url('admin.php?page=ice-booking'); ?>"; }, 1000);
	</script>
		<?php
		return;
	endif;
	
	if(empty($roomtypes)):
		icebooking_class_filter::notification(0, array('Required: Empty Room Type, add it first')); 
		return;
	elseif( wp_verify_nonce( $_POST['add_room_field'], 'add_room_action' ) ):
		icebooking_class_filter::notification($result, $error); 
	endif;	
?>
<h1><?php echo $title?> Rooms</h1>
<form class="form_submit add_room" enctype="multipart/form-data" method="POST" action="">

<table class="list">
<thead>
	<tr>
		<td width="120px">Label</td>
		<td>Value</td>
	</tr>
</thead>
<tbody>
	<tr>
		<td width="120px">Room Type: </td>
		<td>
		<?php if(!empty($roomtypes)): ?>
			<select name="room_type" multiple class="textfield">
			<?php foreach($roomtypes as $roomtype): ?>
				<?php $checked = ($roomtype->id == intval($_GET['id'])) ? 'selected' : '' ;?>
				<option value="<?php echo $roomtype->id?>" <?php echo $checked; ?>><?php echo $roomtype->roomTypeName?></option>
			<?php endforeach; ?>
			</select>
		<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td width="120px">Number Of Childs:</td>
		<td><input type="text" name="number_of_childs" value="<?php echo $input['number_of_childs']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Capacity:</td>
		<td><input type="text" name="capacity_number" value="<?php echo $input['capacity_number']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Number of Rooms:</td>
		<td><input type="text" name="number_of_rooms" value="<?php echo $input['number_of_rooms']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Conditions:</td>
		<td>
	<?php if(!empty($conditions)):?>
		<?php foreach($conditions as $v_con): ?>
			<?php if(!empty($input['conditions_id']))
					$checked = (in_array($v_con->id, $input['conditions_id'])) ? 'checked' : ''; 
			?>
			<input type="checkbox" name="conditions[]" value="<?php echo $v_con->id;?>" <?php echo $checked; ?>/><?php echo $v_con->conditionName;?><br>
		<?php endforeach; ?>
	<?php endif; ?>	
	<?php wp_nonce_field('add_room_action','add_room_field'); ?>
		</td>
	</tr>
	<tr>
		<td>Booking Price:</td>
		<td><input type="text" name="booking_price" value="<?php echo $input['booking_price']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Regular Price:</td>
		<td><input type="text" name="regular_price" value="<?php echo $input['regular_price']?>" class="textfield" /></td>
	</tr>
	<!--tr>
		<td>Rating:</td>
		<td><input type="text" name="room_rating" value="<?php echo $input['room_rating']?>" class="textfield" /></td>
	</tr-->
	<tr>
		<td></td>
		<td>
	<?php wp_nonce_field('add_room_action','add_room_field'); ?>
	<input type="submit" class="textfield submit" value="<?php echo $title; ?> Room" />
		</td>
	</tr>
</tbody>
</table>
</form>
</div>
<?php 
	}
	
	#Function Add Constants
	public function add_room_constants()
	{
		global $wpdb;
		
		$amenities  =  $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_amenities");
		$conditions =  $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_conditions");
		$url  = '';
		$text = 'Add';
		
		if( $_GET['constants'] == 'amenities' 
		    OR  wp_verify_nonce( $_POST['add_amenities_field'], 'add_amenities_action' ) ):
				$table = $wpdb->prefix.'ice_amenities';
				$format = array( '%s','%s' );
			if($_GET['action'] == 'delete'):
				icebooking_class::delete_constants($table, intval($_GET['id']));
			endif;
		endif; 
		
		if( $_GET['constants'] == 'conditions' 
		    OR  wp_verify_nonce( $_POST['add_conditions_field'], 'add_conditions_action' ) ):
				$table = $wpdb->prefix.'ice_conditions';
				$format = array( '%s' );
			if($_GET['action'] == 'delete'):
			endif;
		endif;
		
		#reference
		if(isset($_GET['action']))
			$text = ucwords($_GET['action']);
		
?>

<div class="ice_booking">
<h1>Add Constants</h1>
<hr />
<!-- Amenities -->
<h2>Amenities</h2>
<table id="amenities" class="list">
<?php if($_GET['constants'] == 'amenities' OR !isset($_GET['constants'])): ?>
	<thead class="add">
	<tr class="<?php echo strtolower($text); ?>">
		<td colspan="4">
<?php if($text != 'Add'):?>
	<a class="button addnew" href="<?php echo admin_url('admin.php?page=add-rooms-constants#amenities'); ?>">Add New</a>
<?php endif; ?>
<span class="message"></span>
<form class="ice_form_submit" enctype="multipart/form-data" method="POST" action="<?php echo admin_url('admin.php?page=add-rooms-constants') ?>">
	<?php if($_REQUEST['icon']): ?>
		<img class="preview" src="<?php echo $_REQUEST['icon']?>" width='40px'/>
	<?php endif; ?>
	<input type="text" required="required" id="amenities_icon" class="icon textfield" placeholder="Select Icon:" value="<?php echo $_REQUEST['icon']?>" name="icon"/>
	<input type="text" required="required" name="name" placeholder="Amenity Name: " class="textfield"            value="<?php echo $_REQUEST['name']?>" />
	<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>" />
	<input type="hidden" name="form_action" value="<?php echo $_REQUEST['action']?>" />
	<input type="hidden" name="action" value="jaction" />
	<?php wp_nonce_field('add_amenities_action','add_amenities_field'); ?>
	<input type="submit" class="textfield submit" value="<?php echo $text ?> Amenities"/>
</form>
		</td>
	</tr>
	</thead>
<?php endif; ?>
	<thead>
	<tr>
		<td style="width: 50px;"></td>
		<td style="width: 50px;">Icon</td>
		<td>Name</td>
		<td style="width: 50px;"></td>
	</tr>
	</thead>
	<?php if(!empty($amenities)): ?>
	<tbody>
	<?php foreach($amenities as $amenity): ?>
	<tr id="tr-<?php echo $amenity->id; ?>">
		<td>
			<a onclick="delete_item(this);" class="delete" href="<?php echo admin_url('admin.php?page=add-rooms-constants&constants=amenities&action=delete&id='.$amenity->id)?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/delete.png'; ?>" title="delete" alt="delete" style="width:20px;" />
			</a>	
		</td>
		<td>
			<img src="<?php echo $amenity->icon ?>" width="20px" title="<?php echo $amenity->name ?>" alt="<?php echo $amenity->name ?>" />
		</td>
		<td><?php echo $amenity->name ?></td>
		<td>
			<a class="update" href="<?php echo admin_url("admin.php?page=add-rooms-constants&constants=amenities&action=update&id={$amenity->id}&name={$amenity->name}&icon={$amenity->icon}#amenities")?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/edit.png'; ?>" title="edit" alt="edit" style="width:20px;" />
			</a>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	<?php endif; ?>
</table><br /><br />

<h2>Conditions</h2>
<table id="conditions" class="list">
<?php if($_GET['constants'] == 'conditions' OR !isset($_GET['constants'])): ?>
	<thead class="add">
	<tr class="<?php echo strtolower($text); ?>">
		<td colspan="4">
<?php if($text != 'Add'):?>
	<a class="button addnew" href="<?php echo admin_url('admin.php?page=add-rooms-constants#conditions'); ?>">Add New</a>
<?php endif; ?>
<span class="message"></span>
<form class="ice_form_submit" enctype="multipart/form-data" method="POST" action="<?php echo admin_url('admin.php?page=add-rooms-constants') ?>">
	<input type="text" required="required" name="condition_name" placeholder="Condition Name: " class="textfield" value="<?php echo $_REQUEST['condition_name']?>" />
	<input type="hidden" name="action" value="jaction" />
	<input type="hidden" name="form_action" value="<?php echo $_REQUEST['action']?>" />
	<input type="hidden"  required="required" name="id" value="<?php echo $_REQUEST['id']?>" />
	<?php wp_nonce_field('add_conditions_action','add_conditions_field'); ?>
	<input type="submit" class="textfield submit" value="<?php echo $text ?> Condition"/>
</form>
		</td>
	</tr>
	</thead>
	<thead>
<?php endif; ?>
	<tr>
		<td style="width: 50px;"></td>
		<td>Condition Name</td>
		<td style="width: 50px;"></td>
	</tr>
	</thead>
	<?php if(!empty($conditions)): ?>
	<tbody>
	<?php foreach($conditions as $condition): ?>
	<tr id="con_tr-<?php echo $condition->id; ?>">
		<td>
			<a onclick="delete_item(this);" class="delete" href="<?php echo admin_url('admin.php?page=add-rooms-constants&constants=conditions&action=delete&id='.$condition->id)?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/delete.png'; ?>" title="delete" alt="delete" style="width:20px;" />
			</a>	
		</td>
		<td><?php echo $condition->conditionName ?></td>
		<td>
			<a class="update" href="<?php echo admin_url("admin.php?page=add-rooms-constants&constants=conditions&action=update&id={$condition->id}&condition_name={$condition->conditionName}#conditions")?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/edit.png'; ?>" title="edit" alt="edit" style="width:20px;" />
			</a>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	<?php endif; ?>
</table><br /><br />


</div>
<?php 
	}
	
	#process form submission
	public function process_ajax_form()
	{
		global $wpdb;
		$error = array();
		
		if(urldecode($_POST['purpose']) == 'blocked'):
			$desc = array('desc'=>urldecode($_POST['reason']));
			$wpdb->insert( $wpdb->prefix.'ice_bookcalendar', 
			array( 'startDate'   => $_POST['start'],
				   'endDate'     => $_POST['end'],
				   'description' => serialize($desc),
				   'bkrf' 	 =>  10 ), 
			array( '%s','%s','%s','%d' ) );
			echo $wpdb->insert_id;
			die();
		//admin personnel
		elseif(urldecode($_POST['purpose']) == 'admin'):
			$desc = array('desc'=>urldecode($_POST['reason']),
						  'by'=>urldecode($_POST['personel']),
						  'create_by'=>get_current_user_id()
						  );
			$wpdb->insert( $wpdb->prefix.'ice_bookcalendar', 
			array( 'startDate'   => $_POST['start'],
				   'endDate'     => $_POST['end'],
				   'description' => serialize($desc),
				   'bkrf' 	 =>  9 ), 
			array( '%s','%s','%s','%d' ) );
			echo $wpdb->insert_id;
			die();
		//customer
		elseif(urldecode($_POST['purpose']) == 'customer'):
			//create a customer first
			$wpdb->insert( $wpdb->prefix.'ice_customers', 
			array( 'firstName' => urldecode($_POST['fname']),
				   'lastName'  => urldecode($_POST['lname']),
				   'email'     => urldecode($_POST['email']),
				   'address'   => urldecode($_POST['addr'])), 
			array( '%s','%s','%s','%s' ) );
			$wpdb->insert_id;
			
			//insert booking
			$desc = array('create_by'=>get_current_user_id());
			if(!empty($_POST['rooms'])):
				foreach($_POST['rooms'] as $rm):
			$wpdb->insert( $wpdb->prefix.'ice_bookcalendar', 
			array( 'startDate'   => $_POST['start'],
				   'endDate'     => $_POST['end'],
				   'status'      => urldecode($_POST['stats']),
				   'bkrf' 	 =>  8 ), 
			array( '%s','%s','%d' ) );
				endforeach;
				echo 1;
			else:
				echo 0;
			endif;
			die();
		endif; 
		
		if( wp_verify_nonce( $_POST['add_amenities_field'], 'add_amenities_action' ) ):
			$table = $wpdb->prefix.'ice_amenities';
				$data  = array('name'=>sanitize_text_field($_POST['name']),
					           'icon'=>$_POST['icon']); 
				$format = array( '%s','%s' );
			if($_POST['form_action'] == 'delete'):
				$result = icebooking_class::delete_constants($table, intval($_POST['id']));
			elseif($_POST['form_action'] == 'update'):
				$result = icebooking_class::update_constants($table, $data, intval($_POST['id']), $format);
			elseif( wp_verify_nonce( $_POST['add_amenities_field'], 'add_amenities_action' ) ):
				$result = icebooking_class::add_constants($table, $data, $format);
			endif;
			
			if(!$result)
				$error[] = 'unable to insert data';
			
			echo json_encode(array('error'=>$error, 'success'=>true));
			die();
		endif; 
		
		if( wp_verify_nonce( $_POST['add_conditions_field'], 'add_conditions_action' ) ):
			$table = $wpdb->prefix.'ice_conditions';
				$data   = array('conditionName'=>sanitize_text_field($_REQUEST['condition_name'])); 
				$format = array( '%s' );
			if($_POST['form_action'] == 'delete'):
				$result = icebooking_class::delete_constants($table, intval($_POST['id']));
			elseif($_POST['form_action'] == 'update'):
				$result = icebooking_class::update_constants($table, $data, intval($_POST['id']), $format);
			elseif( wp_verify_nonce( $_POST['add_conditions_field'], 'add_conditions_action' ) ):
				$result = icebooking_class::add_constants($table, $data, $format);
			endif;
			
			if(!$result)
				$error[] = 'unable to insert data';
			
			echo json_encode(array('error'=>$error, 'success'=>true));
			die();
		endif;
	}
	
	#Function Calendar
	function ice_calendar()
	{
		$calendar = new icebooking_class_calendar();
	}
}
endif;
#END