<?php 
#Ice Booking Class Options
if(!class_exists('icebooking_class_calendar')):
class icebooking_class_calendar 
{
	public function __construct()
	{
		$this->calendar();
	}
	public function calendar()
	{
		global $wpdb;
		
		$booked = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_bookcalendar");
		
		if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
		if (!isset($_REQUEST["year"])) $_REQUEST["year"]   = date("Y");

		$current_month = $_REQUEST["month"]; $current_year = $_REQUEST["year"];
		$prev_year     = $current_year;      $next_year    = $current_year;
		$prev_month    = $current_month-1;   $next_month   = $current_month+1;
		
		if ($prev_month == 0 ) {
			$prev_month = 12;
			$prev_year = $current_year - 1;
		}

		if ($next_month == 13 ) {
			$next_month = 1;
			$next_year = $current_year + 1;
		}
?>
<div class="ice_booking">

<h1>Ice Calendar</h1>
<?php if($_GET['views'] == 'peryear'): ?>
<a class="button" href="?page=ice-calendar">View Per Month</a>
<?php 
		$nav_curr_year = (!isset($_REQUEST["year"])) ? date('Y') : $_REQUEST["year"];
		$nav_prev_year    = $nav_curr_year-1;      
		$nav_next_year    = $nav_curr_year+1;
?>
<table width="100%" class="nav">
	<thead>
	<tr>
		<td width="25%" align="left"> <a href='<?php echo admin_url("admin.php?page=ice-calendar&views=peryear&year={$nav_prev_year}");?>'>Previous</a></td>
		<td width="50%" align="center"><h2><?php echo $nav_curr_year; ?></h2></td>
		<td width="25%" align="right"><a href='<?php echo admin_url("admin.php?page=ice-calendar&views=peryear&year={$nav_next_year}");?>'>Next</a>  </td>
	</tr>
	</thead>
</table>
<?php else:  ?>
<a class="button" href="?page=ice-calendar&views=peryear">View Per Year</a>
<!--table width="100%" class="nav">
	<thead>
	<tr>
		<td width="50%" align="left"> <a href='<?php echo admin_url("admin.php?page=ice-calendar&month={$prev_month}&year={$prev_year}");?>'>Previous</a></td>
		<td width="50%" align="right"><a href='<?php echo admin_url("admin.php?page=ice-calendar&month={$next_month}&year={$next_year}");?>'>Next</a>  </td>
	</tr>
	</thead>
</table-->
<?php endif;  ?>
<?php 
	#var_dump(date("F j, Y, g:i a",strtotime($booked[0]->startDate)));
	#var_dump(strtotime($booked[0]->startDate));
	
	$booked_date = array();
	#for($i=0;$i<15;$i++):
	foreach($booked as $kbooked => $vbooked):
		$user_info = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ice_customers WHERE id={$vbooked->customerId}");
		$title = "Reservation from {$user_info->email}";
			if($vbooked->status == 10)
				$title = "Blocked room# {$vbooked->roomId}";
		$booked_date[] = array('id'		=> $vbooked->customerId, 
							   'title'	=> $title,
							   'status'	=> $vbooked->status,
							   'user'	=> $user_info,
							   'start'	=> date("Y-m-d",strtotime($vbooked->startDate)),
							   'end'	=> date("Y-m-d",strtotime($vbooked->endDate)));
		#$booked_date[strtotime(date("Y-m-d",strtotime($vbooked->startDate)))] = $vbooked->customerId; 
		#$booked_date[strtotime(date("Y-m-d",strtotime($vbooked->endDate)))]   = $vbooked->customerId; 
	endforeach;
	#endfor;
?>		
<script type="text/javascript">
	var booking_sched = <?php echo json_encode($booked_date); ?>
</script>
<?php 		
#print_r($booked_date);	
if($_GET['views']=='peryear'):	
	$this->calendar_year(array('current_month'=>$current_month,
							    'current_year'=>$current_year,
								'prev_year'=>$prev_year,
								'prev_month'=>$prev_month,
								'next_year'=>$next_year,
								'next_month'=>$next_month,
								'booked_date'=>$booked_date));
else:
	/*$this->calendar_month(array('current_month'=>$current_month,
							    'current_year'=>$current_year,
								'prev_year'=>$prev_year,
								'prev_month'=>$prev_month,
								'next_year'=>$next_year,
								'next_month'=>$next_month,
								'booked_date'=>$booked_date));*/
?>
<div id="calendar"></div>
<?php endif; ?>

</div>
<?php
	}
	
	#Calendar by month
	protected function calendar_month($parameters=false)
	{
		global $wpdb;
		if(!$parameters)
			return false;
			
		$month_names = Array("January", "February", "March", "April", "May", "June", "July", 
						     "August", "September", "October", "November", "December");
							 
		foreach($parameters as $kpar => $vpar):
			${$kpar} = $vpar;
		endforeach;
		
		$timestamp  = mktime(0,0,0,$current_month,1,$current_year);
		$maxday     = date("t",$timestamp);
		$thismonth  = getdate($timestamp);
		$startday   = $thismonth['wday'];
		$calendar_title = $month_names[$current_month-1].' '.$current_year;
		$calendar_title = ($_GET['views']=='peryear') ? "<a href='?page=ice-calendar&month={$current_month}&year={$current_year}'>{$calendar_title}</a>" : $calendar_title;
?>

<?php 
	#$calendar_title = "<a href='?page=ice-calendar&month={$current_month}&year={$current_year}'>{$calendar_title}</a>";
?>
<div class="cal_content">
</div>
		<table class="calendar list" width="100%" style="position:relative">
		<thead>
		<tr height="50px"  align="center">
			<td colspan="7"><strong><?php echo $calendar_title; ?></strong></td>
		</tr>
		<tr height="50px" >
			<td><strong>SUN</strong></td>
			<td><strong>MON</strong></td>
			<td><strong>TUE</strong></td>
			<td><strong>WED</strong></td>
			<td><strong>THU</strong></td>
			<td><strong>FRI</strong></td>
			<td><strong>SAT</strong></td>
		</tr>
		</thead>
		<tbody>
<?php 

for ($i=0; $i <($maxday+$startday); $i++) {
	$day_number = ($i - $startday + 1);
	$day_numbertime = strtotime($current_year.'-'.$current_month.'-'. $day_number);
	
	if(($i % 7) == 0 ) echo "<tr>";
    if($i < $startday) { 
		echo "<td></td>";
    } else {
		echo "<td class='jtd_day' data-date='{$current_year}-{$current_month}-{$day_number}'>". $day_number;
		#$cid = array_search($day_numbertime, $booked_date);
		#if($day_numbertime==$booked_date[$cid][0]):
		/*if(in_array($day_numbertime, array_keys($booked_date))):
			$user_info = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ice_customers WHERE id = {$booked_date[$day_numbertime]}");
			echo '<td class="booked">';
			echo $day_number.
			     '<br />
				 <a href="#TB_inline?height=150px&width=200&inlineId=user_'.$user_info->id.'" class="thickbox" title="'.$user_info->firstName.' '.$user_info->lastName.'">
					<img src="'.plugins_url("ice-booking")."/images/icons/human.png".'" />
				 </a>';*/
?>
<!--div id="user_<?php echo $user_info->id?>" style="display:none;">
<p>
	First Name: <strong><?php echo $user_info->firstName ;?> </strong><br />
	Last Name: 	<strong><?php echo $user_info->lastName ;?> </strong><br />
	Email Add: 	<strong><?php echo $user_info->email ;?> </strong><br />
	Phone1: 	<strong><?php echo $user_info->phone1 ;?> </strong><br />
	Phone2: 	<strong><?php echo $user_info->phone2 ;?> </strong><br />
	SMS Code: 	<strong><?php echo $user_info->smsCode ;?> </strong><br />
</p>
</div-->
<?php				  
			#foreach($booked_date as $kbok => $vbok):
			#if($vbok == $day_numbertime):
				#$user_info = $wpdb->get_row("SELECT * FROM a_customers WHERE id = {$cid}");
				#echo "<br />{$user_info->email}";
			#endif;
			#endforeach;
		/*else:
			echo "<td>". $day_number;
		endif;
		
		echo "</td>";*/
	}
    if(($i % 7) == 6 ) echo "</tr>";
}
?>
		</tbody>
		</table>
		
<?php
	}
	
	#Calendar by week
	protected function calendar_year($parameters=false)
	{
		global $wpdb;
		
		if(!$parameters)
			return false;
		
?>

	<?php for($i=1;$i<13;$i++):?>
	<?php 
		$parameters = array_replace($parameters, array('current_month'=>$i));
	?>
	<div class="mini">
		<?php $this->calendar_month($parameters);?>
	</div>
		<?php if(($i%3)==0): ?>
			<div style="clear:both"></div>
		<?php endif; ?>
	<?php endfor;?>
<?php		
		return;
	}
	
	#Javascript Calendar
	function js_calendar_month()
	{
		
	}
}
endif;
#END