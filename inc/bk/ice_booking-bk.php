<?php 
#Ice Booking Class
if(!class_exists('icebooking_class')):
class icebooking_class 
{
	public function create_ice_booking_tables()
	{
		global $wpdb;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		#$charset_collate = '';
		#if ( ! empty( $wpdb->charset ) ) {  $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}"; }
		#if ( ! empty( $wpdb->collate ) ) {  $charset_collate .= " COLLATE {$wpdb->collate}";}

		#Amenities
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_amenities (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					name varchar(255) NOT NULL,
					icon varchar(255) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
		dbDelta( $sql );
		
		#Conditions
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_conditions (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					conditionName varchar(255) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Capacities
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_capacities (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					capacityNr int(11) NOT NULL,
					capacityName varchar(255) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
		dbDelta( $sql );
		
		#Rooms
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_rooms (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomTypeId int(11) NOT NULL,
					roomNr varchar(40) NOT NULL,
					capacityId int(11) NOT NULL,
					totalChilds int(11) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Room Type
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_roomtype (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomTypeName varchar(255) NOT NULL,
					description text NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Room Type Amenities
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_roomtypeamenities (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					amenentiesId int(11) NOT NULL,
					roomTypeId int(11) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Room Type Conditions
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_roomtypeconditions (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomTypeId int(11) NOT NULL,
					conditionId int(11) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Room Type Media
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_roomtypemedia (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomTypeId int(11) NOT NULL,
					image varchar(255) NOT NULL,
					video text NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
	}
	
	#ADD ROOM
	function add_room($data = false) {
		global $wpdb;
		if(!$data)
			return false;
		
		$wpdb->insert( $wpdb->prefix.'ice_roomtype', 
			array( 'roomTypeName' => $data['room_name'], 
				   'description' =>  $data['description'] ), 
			array( '%s','%s' ) );
		$roomType = $wpdb->insert_id;
		
	#insert room type amenities
	if(!empty($data['amenities'])):
	foreach($data['amenities'] as $amenities):
		$wpdb->insert( $wpdb->prefix.'ice_roomtypeamenities', 
			array( 'amenentiesId' => $amenities,
				   'roomTypeId'   => $roomType), 
			array( '%d', '%d' ) );
	endforeach;
	endif;
	
	#insert room type conditions
	if(!empty($data['conditions'])):
	foreach($data['conditions'] as $conditions):
		$wpdb->insert( $wpdb->prefix.'ice_roomtypeconditions', 
			array( 'conditionId' => $conditions,
				   'roomTypeId'   => $roomType), 
			array( '%d', '%d' ) );
	endforeach;
	endif;
		
	#insert room type media
	if(!empty($data['media'])):
		$wpdb->insert( $wpdb->prefix.'ice_roomtypemedia', 
			array( 'image'        => $data['media']['image'], 
				   'video'        => $data['media']['video'], 
				   'roomTypeId'   => $roomType ), 
			array( '%s', '%s','%d' ) );
	endif;
		
		$wpdb->insert( $wpdb->prefix.'ice_rooms', 
			array( 'roomTypeId' => $roomType, 
				   'roomNr'     => $data['room_number'] ,
				   'capacityId' => $data['capacity_id'],
				   'totalChilds'     => $data['number_of_childs']
				   ), 
			array( '%d', '%d', '%d', '%d' ) );
		
		return true;
	}
	
	#DELETE ROOM
	function delete_room($id) {
		global $wpdb;
		
		/*$wpdb->query("DELETE ".$wpdb->prefix."ice_rooms, 
						".$wpdb->prefix."ice_roomtype,
						".$wpdb->prefix."ice_roomtypeamenities,
						".$wpdb->prefix."ice_roomtypeconditions,
						".$wpdb->prefix."ice_roomtypemedia 
					  FROM  ".$wpdb->prefix."ice_rooms, 
						".$wpdb->prefix."ice_roomtype,
						".$wpdb->prefix."ice_roomtypeamenities,
						".$wpdb->prefix."ice_roomtypeconditions,
						".$wpdb->prefix."ice_roomtypemedia
	WHERE ".$wpdb->prefix."ice_roomtype.id  = ".$wpdb->prefix."ice_rooms.id  AND
		  ".$wpdb->prefix."ice_roomtypeamenities.roomTypeId  = ".$wpdb->prefix."ice_roomtype.id AND
		  ".$wpdb->prefix."ice_roomtypeconditions.roomTypeId = ".$wpdb->prefix."ice_roomtype.id AND
          ".$wpdb->prefix."ice_roomtypemedia.roomTypeId = ".$wpdb->prefix."ice_roomtype.id AND
		  ".$wpdb->prefix."ice_roomtype.id = ".$id.";");*/
		
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_rooms WHERE id={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtype WHERE id={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtypeamenities WHERE roomTypeId={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtypeconditions WHERE roomTypeId={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtypemedia WHERE roomTypeId={$id}");
		return true; 
	}
	
	/*#UPDATE ROOM
	function update_room($id, $data = false) {
		global $wpdb;
		if(!$id OR !$data)
			return false;
	
	#update room type
		$wpdb->update( $wpdb->prefix.'ice_roomtype', 
			array( 'roomTypeName' => $data['room_name'], 
				   'description' =>  $data['description'] ),
			array( 'id' => $id), 
			array( '%s','%s' ) );
		
	
	#insert room type amenities
	if(!empty($data['amenities'])):
	$delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."ice_roomtypeamenities 
				  WHERE roomTypeId = {$id};");
		if($delete):
	foreach($data['amenities'] as $amenities):
		$wpdb->insert( $wpdb->prefix.'ice_roomtypeamenities', 
			array( 'amenentiesId' => $amenities,
				   'roomTypeId'   => $id), 
			array( '%d', '%d' ) );
	endforeach;
		endif;
	endif;
	
	#insert room type conditions
	if(!empty($data['conditions'])):
	$delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."ice_roomtypeconditions 
				  WHERE roomTypeId = {$id};");
		if($delete):
	foreach($data['conditions'] as $conditions):
		$wpdb->insert( $wpdb->prefix.'ice_roomtypeconditions', 
			array( 'conditionId' => $conditions,
				   'roomTypeId'   => $id), 
			array( '%d', '%d' ) );
	endforeach;
		endif; 
	endif;
		
	#insert room type media
	if(!empty($data['media'])):
	$delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."ice_roomtypemedia 
				  WHERE roomTypeId = {$id};");
			if($delete):
		$wpdb->insert( $wpdb->prefix.'ice_roomtypemedia', 
			array( 'image'        => $data['media']['image'], 
				   'video'        => $data['media']['video'], 
				   'roomTypeId'   => $id ), 
			array( '%s', '%s','%d' ) );
			endif;
	endif;
		
		$wpdb->update( $wpdb->prefix.'ice_rooms', 
			array( 'roomTypeId' => $id, 
				   'roomNr'     => $data['room_number'] ,
				   'capacityId' => $data['capacity_id'],
				   'totalChilds'     => $data['number_of_childs']
				   ), 
			array( 'id' => $id),
			array( '%d', '%d', '%d', '%d' ) );
		
		return true;
	}*/
	
	#Add Room Type
	function add_roomtype($data)
	{
		global $wpdb;
		
		#primary table must
		$wpdb->insert( $wpdb->prefix.'ice_roomtype', 
			array( 'roomTypeName' => $data['room_type'], 
				   'description' =>  $data['description'] ), 
			array( '%s','%s' ) );
		$id = $wpdb->insert_id;
						
		#success room type addition
		if($id):
			#insert room type amenities
			/*if(!empty($data['amenities'])):
				foreach($data['amenities'] as $amenities):
					$wpdb->insert( $wpdb->prefix.'ice_roomtypeamenities', 
					array( 'amenentiesId' => $amenities,
						   'roomTypeId'   => $id), 
					array( '%d', '%d' ) );
				endforeach;
			endif;
	
			#insert room type conditions
			if(!empty($data['conditions'])):
				foreach($data['conditions'] as $conditions):
					$wpdb->insert( $wpdb->prefix.'ice_roomtypeconditions', 
					array( 'conditionId' => $conditions,
						   'roomTypeId'  => $id), 
					array( '%d', '%d' ) );
				endforeach;
			endif;*/
		
			#insert room type media
			if(!empty($data['media'])):
				$wpdb->insert( $wpdb->prefix.'ice_roomtypemedia', 
					array( 'image'        => serialize($data['media']['image']), 
						   'video'        => $data['media']['video'], 
						   'roomTypeId'   => $id ), 
					array( '%s', '%s','%d' ) );
			endif;
		endif;
	}
	
	#Add Constants
	function add_constants($table, $data, $format) {
		global $wpdb;
		if(!$data)
			return false;
		
		return $wpdb->insert( $table, $data, $format );
	}
	
	#Delete Constants
	function delete_constants($table, $id) {
		global $wpdb;
		if(!$id)
			return false;
				
		$sql = "DELETE FROM {$table} WHERE id = {$id}";
		return $wpdb->query( $sql );
	}
	
	#Update Constants
	function update_constants($table, $data, $id,  $format) {
		global $wpdb;
		if(!$data)
			return false;
		
		return $wpdb->update( $table, $data, array('id'=>$id),  $format );
	}
	
}
endif;
#END