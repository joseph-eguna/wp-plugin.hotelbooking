<?php 
#Ice Booking Class Options
if(!class_exists('icebooking_class_options')):
class icebooking_class_options 
{
	public function __construct()
	{
		#$this->options_framework();
		return;
	}
	
	public function options_framework()
	{
?>
		<?php if($_GET['booking'] == 'addroom'): ?>
			<?php self::add_room_framework(); ?>
		<?php elseif($_GET['booking'] == 'updateroom'): ?>
			<?php self::add_room_framework(true); ?>
		<?php else: ?>
			<?php if($_GET['booking'] == 'delete')
						icebooking_class::delete_room($_REQUEST['id']); ?>
			<?php self::list_room_framework(); ?>
		<?php endif; ?>
	
<?php
	}
	
	#Function List of room framework
	public function list_room_framework()
	{
		global $wpdb;
		
		$sql = "SELECT * FROM {$wpdb->prefix}ice_rooms";
		$list_of_rooms = $wpdb->get_results($sql);
?>
<div class="ice_booking">
<h1>List Of Rooms</h1>
<table class="list">
	<thead>
	<tr>
		<td width="20px"></td>
		<td width="100px">Name</td>
		<td>Description</td>
		<td width="50px">Capacity</td>
		<td width="50px">Total Childs</td>
		<td width="100px">Amenities</td>
		<td width="100px">Conditions</td>
		<td width="50px">Ratings</td>
		<td width="50px">Price</td>
		<td width="20px"></td>
	</tr>
	</thead>
	<tbody>
	<?php if(!empty($list_of_rooms)):  foreach($list_of_rooms as $k_room => $v_room): ?>
		<?php 
			
			$room_desc 	   = $wpdb->get_row("SELECT * FROM  {$wpdb->prefix}ice_roomtype WHERE id={$v_room->roomTypeId}");
			$room_amen_ids = $wpdb->get_results("SELECT * FROM  {$wpdb->prefix}ice_roomtypeamenities WHERE roomTypeId={$v_room->roomTypeId}");
			$room_cond_ids = $wpdb->get_results("SELECT * FROM  {$wpdb->prefix}ice_roomtypeconditions WHERE roomTypeId={$v_room->roomTypeId}");
			#$room_capa = $wpdb->get_row($sql);
		?>
	<tr>
		<?php
			$update_url = admin_url('admin.php?page=ice-booking&booking=updateroom')."&id={$v_room->id}&room_name={$room_desc->roomTypeName}&description={$room_desc->description}&room_number={$v_room->roomNr}&number_of_childs={$v_room->totalChilds}";
		?>
		<td>
			<a href="<?php echo admin_url('admin.php?page=ice-booking&booking=delete&id='.$v_room->id)?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/delete.png'; ?>" alt="delete" style="width:20px;" />
			</a>
		</td>
		<td><?php echo str_replace('\\', '',$room_desc->roomTypeName); ?></td>
		<td><?php echo str_replace('\\', '',$room_desc->description); ?></td>
		<td></td>
		<td>
			<?php if(intval($v_room->totalChilds)):?>
				<?php for($tC=0;$tC<=$v_room->totalChilds;$tC++):?>
					<img src="<?php echo plugins_url('ice-booking').'/images/icons/human.png'; ?>" width="13px" />
				<?php endfor; ?>
			<?php endif; ?>
		</td>
		<td>
			<?php if(!empty($room_amen_ids)):?>
			<?php foreach($room_amen_ids as $room_amen_id): ?>
				<?php $room_amenities = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ice_amenities WHERE id={$room_amen_id->amenentiesId}"); ?>
				<img src="<?php echo $room_amenities->icon; ?>" alt="<?php echo $room_amenities->name; ?>" title="<?php echo $room_amenities->name; ?>" style="width:25px;" />
			<?php endforeach;  ?>
			<?php endif; ?>
		</td>
		<td>
			<?php if(!empty($room_cond_ids)):?>
			<?php foreach($room_cond_ids as $room_cond_id): ?>
				<?php $room_conditions = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}ice_conditions WHERE id={$room_cond_id->conditionId}"); ?>
				<?php echo $room_conditions->conditionName.'<br />'; ?>
			<?php endforeach;  ?>
			<?php endif; ?>
		</td>
		<td><?php #echo $v_room->roomNr ?></td>
		<td>
			<a href="<?php echo $update_url?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/edit.png'; ?>" alt="edit" style="width:20px;" />
			</a>
		</td>
	</tr>
	
	<?php endforeach; ?>
	</tbody>
	<?php else: ?>
	<tbody>
	<tr>
		<td colspan="9"> No rooms Inserted! </td>
	</tr>
	</tbody>
	<?php endif; ?>
</table>
</div>
<?php
	}
	
	#Function Add form framework
	public function add_room_framework($update = false)
	{
		global $wpdb;
		$sql = "SELECT * FROM {$wpdb->prefix}ice_amenities;";
		$amenities = $wpdb->get_results($sql);
		
		$sql = "SELECT * FROM {$wpdb->prefix}ice_conditions;";
		$conditions = $wpdb->get_results($sql);
		
	if ( wp_verify_nonce( $_POST['add_room_field'], 'add_room_action' ) ):
		$data = array();
		foreach($_POST as $kpost => $vpost):
			if( isset($_POST[$kpost]) 
				AND !empty($_POST[$kpost])
				AND $kpost != 'add_room_field'
				AND $kpost != '_wp_http_referer' )
				$data[$kpost] = $vpost; 
		endforeach;
		$data['media'] =  array('image'=>'Chttp://media.com/media1.jpg',
								'video' =>'Chttp://video.com/video1.swf');
		if($update):
			icebooking_class::update_room($_REQUEST['id'], $data);
		else:
			icebooking_class::add_room($data);
			#var_dump($_POST);
		endif;
	endif;
?>
<?php 
	$url = '&booking=addroom';
	$title = 'Add Room';
	if($update):
		$url = '&booking=updateroom';
		$title = 'Update Room';
	else:
		unset($_REQUEST);
	endif; 
	
?>
<div class="ice_booking">
<h1><?php echo $title?></h1>
<table>
<form class="form_submit add_room" enctype="multipart/form-data" method="POST" action="<?php echo admin_url('admin.php?page=ice-booking'.$url) ?>">
<tbody>
	<tr>
		<td colspan="2" style="vertical-align:top">
<table class="list">
<thead>
	<tr>
		<td width="120px">Label</td>
		<td>Value</td>
	</tr>
</thead>
<!-- Column 1-->
<tbody>
	<tr>
		<td>Room name: </td>
		<td><input type="text" name="room_name"  required="required" value="<?php echo str_replace('\\', '',$_REQUEST['room_name']); ?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Room Description:</td>
		<td>
			<textarea required="required" name="descrption"<?php echo str_replace('\\', '',$_REQUEST['description']); ?>" class="textfield"></textarea>
		</td>
	</tr>
	<tr>
		<td>Number Of Childs:</td>
		<td><input type="text" name="number_of_childs" value="<?php echo $_REQUEST['number_of_childs']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Capacity Number:</td>
		<td><input type="text" name="capacity_number" value="<?php echo $_REQUEST['capacity_number']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Images:</td>
		<td><input id="room_image" type="text" name="images" value="<?php echo $_REQUEST['images']?>" class="icon textfield" /></td>
	</tr>
	<tr>
		<td>Videos:</td>
		<td><input type="text" name="videos" value="<?php echo $_REQUEST['videos']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Amenities:</td>
		<td>
		<?php if(!empty($amenities)):?>
			<?php foreach($amenities as $v_amen): ?>
			<span class="block">
				<input type="checkbox" name="amenities[]" value="<?php echo $v_amen->id;?>">
				<img src="<?php echo $v_amen->icon;?>" alt="<?php echo $v_amen->name;?>" title="<?php echo $v_amen->name;?>" width="25px" />
			</span>
			<?php endforeach; ?>
		<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td>Conditions:</td>
		<td>
	<?php if(!empty($conditions)):?>
		<?php foreach($conditions as $v_con): ?>
			<input type="checkbox" name="conditions[]" value="<?php echo $v_con->id;?>"><?php echo $v_con->conditionName;?><br>
		<?php endforeach; ?>
	<?php endif; ?>	
	<?php wp_nonce_field('add_room_action','add_room_field'); ?>
	<?php if($update):?>
		<input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>" />
	<?php endif; ?>
		</td>
	</tr>
</tbody>
</table>		
		</td>
		<td colspan="2" style="vertical-align:top">
<!-- Column2 -->
<table class="list">
<thead>
	<tr>
		<td width="120px">Label</td>
		<td>Value</td>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Booking Price:</td>
		<td><input type="text" name="room_number" value="<?php echo $_REQUEST['sale_price']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Regular Price:</td>
		<td><input type="text" name="room_number" value="<?php echo $_REQUEST['regular_price']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td>Rates:</td>
		<td><input type="text" name="room_number" value="<?php echo $_REQUEST['rates']?>" class="textfield" /></td>
	</tr>
	<tr>
		<td></td>
		<td>
	<input type="submit" class="textfield submit" value="<?php echo $title;?>" />
		</td>
	</tr>
</tbody>
</table>
		</td>
	</tr>
</tbody>
</form>
</table>
</div>
<?php 
	}
	
	#Function Add Constants
	public function add_room_constants()
	{
		global $wpdb;
		
		$amenities  =  $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_amenities");
		$conditions =  $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ice_conditions");
		$url  = '';
		$text = 'Add';
		
		if( $_GET['constants'] == 'amenities' 
		    OR  wp_verify_nonce( $_POST['add_amenities_field'], 'add_amenities_action' ) ):
			$table = $wpdb->prefix.'ice_amenities';
				$data  = array('name'=>sanitize_text_field($_REQUEST['name']),
					           'icon'=>sanitize_text_field($_REQUEST['icon'])); 
				$format = array( '%s','%s' );
			if($_GET['action'] == 'delete'):
				icebooking_class::delete_constants($table, intval($_GET['id']));
			elseif($_GET['action'] == 'update'):
				icebooking_class::update_constants($table, $data, intval($_GET['id']), $format);
			elseif( wp_verify_nonce( $_POST['add_amenities_field'], 'add_amenities_action' ) ):
				icebooking_class::add_constants($table, $data, $format);
			endif;
		endif; 
		
		if( $_GET['constants'] == 'conditions' 
		    OR  wp_verify_nonce( $_POST['add_conditions_field'], 'add_conditions_action' ) ):
			$table = $wpdb->prefix.'ice_conditions';
				$data   = array('conditionName'=>sanitize_text_field($_REQUEST['name'])); 
				$format = array( '%s' );
			if($_GET['action'] == 'delete'):
				icebooking_class::delete_constants($table, intval($_GET['id']));
			elseif($_GET['action'] == 'update'):
				icebooking_class::update_constants($table, $data, intval($_GET['id']), $format);
			elseif( wp_verify_nonce( $_POST['add_conditions_field'], 'add_conditions_action' ) ):
				icebooking_class::add_constants($table, $data, $format);
			endif;
		endif;
		
		#reference
		if(isset($_GET['action'])):
			$text = ucwords($_GET['action']);
			if($_GET['action'] == 'update'):
				$url = "&constants=".$_GET['constants']."&action=update&id=".intval($_GET['id']);
			endif;
		else:
			unset($_REQUEST);
		endif;
?>

<div class="ice_booking">
<h1>Add Constants</h1>
<hr />
<!-- Amenities -->
<h2>Amenities</h2>
<table id="amenities" class="list">
	<thead class="add">
	<tr class="<?php echo strtolower($text); ?>">
		<td colspan="4">
<?php if($text != 'Add'):?>
	<a class="button addnew" href="<?php echo admin_url('admin.php?page=add-rooms-constants#amenities'); ?>">Add New</a>
<?php endif; ?>
<span class="message"></span>
<form class="ice_form_submit" enctype="multipart/form-data" method="POST" action="<?php echo admin_url('admin.php?page=add-rooms-constants'.$url) ?>">
	<?php if($_REQUEST['icon']): ?>
		<img class="preview" src="<?php echo $_REQUEST['icon']?>" width='40px'/>
	<?php endif; ?>
	<input type="text" required="required" id="amenities_icon" class="icon textfield" placeholder="Select Icon:" value="<?php echo $_REQUEST['icon']?>" name="icon"/>
	<input type="text" required="required" name="name" placeholder="Amenity Name: " class="textfield"            value="<?php echo $_REQUEST['name']?>" />
	<input type="hidden" name="id" class="textfield" value="<?php echo $_REQUEST['id']?>" />
	<?php wp_nonce_field('add_amenities_action','add_amenities_field'); ?>
	<input type="submit" class="textfield submit" value="<?php echo $text ?> Amenities"/>
</form>
		</td>
	</tr>
	</thead>
	<thead>
	<tr>
		<td style="width: 50px;"></td>
		<td style="width: 50px;">Icon</td>
		<td>Name</td>
		<td style="width: 50px;"></td>
	</tr>
	</thead>
	<?php if(!empty($amenities)): ?>
	<tbody>
	<?php foreach($amenities as $amenity): ?>
	<tr id="tr-<?php echo $amenity->id; ?>">
		<td>
			<a onclick="delete_item(this);" class="delete" href="<?php echo admin_url('admin.php?page=add-rooms-constants&constants=amenities&action=delete&id='.$amenity->id)?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/delete.png'; ?>" title="delete" alt="delete" style="width:20px;" />
			</a>	
		</td>
		<td>
			<img src="<?php echo $amenity->icon ?>" width="20px" title="<?php echo $amenity->name ?>" alt="<?php echo $amenity->name ?>" />
		</td>
		<td><?php echo $amenity->name ?></td>
		<td>
			<a class="update" href="<?php echo admin_url("admin.php?page=add-rooms-constants&constants=amenities&action=update&id={$amenity->id}&name={$amenity->name}&icon={$amenity->icon}#amenities")?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/edit.png'; ?>" title="edit" alt="edit" style="width:20px;" />
			</a>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	<?php endif; ?>
</table><br /><br />

<h2>Conditions</h2>
<table id="conditions" class="list">
	<thead class="add">
	<tr class="<?php echo strtolower($text); ?>">
		<td colspan="4">
<?php if($text != 'Add'):?>
	<a class="button addnew" href="<?php echo admin_url('admin.php?page=add-rooms-constants#conditions'); ?>">Add New</a>
<?php endif; ?>
<span class="message"></span>
<form class="ice_form_submit" enctype="multipart/form-data" method="POST" action="<?php echo admin_url('admin.php?page=add-rooms-constants'.$url) ?>">
	<input type="text" required="required" name="name" placeholder="Condition Name: " class="textfield" value="<?php echo $_REQUEST['name']?>" />
	<input type="hidden"  required="required" name="id" class="textfield" value="<?php echo $_REQUEST['id']?>" />
	<?php wp_nonce_field('add_conditions_action','add_conditions_field'); ?>
	<input type="submit" class="textfield submit" value="<?php echo $text ?> Condition"/>
</form>
		</td>
	</tr>
	</thead>
	<thead>
	<tr>
		<td style="width: 50px;"></td>
		<td>Condition Name</td>
		<td style="width: 50px;"></td>
	</tr>
	</thead>
	<?php if(!empty($conditions)): ?>
	<tbody>
	<?php foreach($conditions as $condition): ?>
	<tr id="con_tr-<?php echo $condition->id; ?>">
		<td>
			<a onclick="delete_item(this);" class="delete" href="<?php echo admin_url('admin.php?page=add-rooms-constants&constants=conditions&action=delete&id='.$condition->id)?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/delete.png'; ?>" title="delete" alt="delete" style="width:20px;" />
			</a>	
		</td>
		<td><?php echo $condition->conditionName ?></td>
		<td>
			<a class="update" href="<?php echo admin_url("admin.php?page=add-rooms-constants&constants=conditions&action=update&id={$condition->id}&name={$condition->conditionName}#conditions")?>">
				<img src="<?php echo plugins_url('ice-booking').'/images/icons/edit.png'; ?>" title="edit" alt="edit" style="width:20px;" />
			</a>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	<?php endif; ?>
</table><br /><br />


</div>
<?php 
	}
}
endif;
#END