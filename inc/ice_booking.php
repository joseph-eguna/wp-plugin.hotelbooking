<?php 
#Ice Booking Class
if(!class_exists('icebooking_class')):
class icebooking_class 
{
	public function create_ice_booking_tables()
	{
		global $wpdb;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		#$charset_collate = '';
		#if ( ! empty( $wpdb->charset ) ) {  $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}"; }
		#if ( ! empty( $wpdb->collate ) ) {  $charset_collate .= " COLLATE {$wpdb->collate}";}

		#Amenities
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_amenities (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					name varchar(255) NOT NULL,
					icon varchar(255) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
		dbDelta( $sql );
		
		#Conditions
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_conditions (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					conditionName varchar(255) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Capacities
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_capacities (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					capacityNr int(11) NOT NULL,
					capacityName varchar(255) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
		dbDelta( $sql );
		
		#Rooms
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_rooms (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomTypeId int(11) NOT NULL,
					roomNr varchar(40) NOT NULL,
					capacityId int(11) NOT NULL,
					totalChilds int(11) NOT NULL,
					bookingPrice int(11) NOT NULL,
					regularPrice int(11) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Room Type
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_roomtype (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomTypeName varchar(255) NOT NULL,
					description text NOT NULL,
					bedRoom int(11) NOT NULL,
					bathRoom int(11) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Room Type Amenities
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_roomtypeamenities (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					amenentiesId int(11) NOT NULL,
					roomTypeId int(11) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Room Type Conditions
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_roomtypeconditions (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomId int(11) NOT NULL,
					conditionId int(11) NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Room Type Media
		$sql =  "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."ice_roomtypemedia (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomTypeId int(11) NOT NULL,
					image varchar(255) NOT NULL,
					video text NOT NULL,
					PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
		
		#Customers data
		$sql =  "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}ice_customers (
					 id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					 firstName varchar(255) NOT NULL,
					 lastName varchar(255) NOT NULL,
					 email varchar(255) NOT NULL,
					 phone1 varchar(255) NOT NULL,
					 phone2 varchar(255) NOT NULL,
					 smsCode varchar(25) NOT NULL,
					 paymentCode varchar(255) NOT NULL,
					 PRIMARY KEY (id)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );

		#Calendar Booking
		$sql =  "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}ice_bookcalendar (
					id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
					roomId int(11) NOT NULL,
					startDate datetime NOT NULL,
					endDate datetime NOT NULL,
					status tinyint(4) NOT NULL,
					people tinyint(3) NOT NULL,
					customerId int(11) NOT NULL,
					typeOfPayment int(11) NOT NULL,
					PRIMARY KEY (id)
				)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		dbDelta( $sql );
	}
	
	#add room
	function add_room($data = false) {
		global $wpdb;
		if(!$data)
			return false;
		
		$wpdb->insert( $wpdb->prefix.'ice_rooms', 
			array( 'roomTypeId'		=>  $data['room_type'],
				   'roomNr' 		=>  $data['number_of_rooms'], 
				   'capacityId' 	=>  $data['capacity_number'], 
				   'totalChilds' 	=>  $data['number_of_childs'], 
				   'bookingPrice' 	=>  $data['booking_price'], 
				   'regularPrice' 	=>  $data['regular_price'] ), 
			array( '%d','%s','%d','%d','%s','%s' ) );
		$id = $wpdb->insert_id;
		
		if($id):
			if(!empty($data['conditions'])):
				foreach($data['conditions'] as $condition):
					$wpdb->insert( $wpdb->prefix.'ice_roomtypeconditions', 
					array( 'conditionId' => $condition,
						   'roomId'  => $id), 
					array( '%d', '%d' ) );
				endforeach;
			endif;
			return $id;
		endif;
		return false;
	}
	
	#delete room
	function delete_room($id) {
		global $wpdb;
		
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_rooms WHERE id={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtypeconditions WHERE roomId={$id}");
		
		return true; 
	}
	
	#update room
	function update_room($id, $data = false) {
		global $wpdb;
		if(!$data OR !$id)
			return false;
		
		$id = $wpdb->update( $wpdb->prefix.'ice_rooms', 
			array( 'roomTypeId'		=>  $data['room_type'],
				   'roomNr' 		=>  $data['number_of_rooms'], 
				   'capacityId' 	=>  $data['capacity_number'], 
				   'totalChilds' 	=>  $data['number_of_childs'],  
				   'bookingPrice' 	=>  $data['booking_price'], 
				   'regularPrice' 	=>  $data['regular_price'] ), 
			array( 'id' => $id),
			array( '%d','%d','%d','%d','%s','%s' ) );
		
		if($id):
		$delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."ice_roomtypeconditions 
				  WHERE roomId = {$id};");
			if(!empty($data['conditions']) AND $delete):
				foreach($data['conditions'] as $condition):
					$wpdb->insert( $wpdb->prefix.'ice_roomtypeconditions', 
					array( 'conditionId' => $condition,
						   'roomId'  => $id), 
					array( '%d', '%d' ) );
				endforeach;
			endif;
			return $id; 
		endif;
		return false;
	}
	
	#add room type
	function add_roomtype($data)
	{
		global $wpdb;
		
		if(!$data)
			return false;
		
		#primary table must
		$wpdb->insert( $wpdb->prefix.'ice_roomtype', 
			array( 'roomTypeName' => $data['room_type'],
				   'bedRoom'      => $data['bed_size'],
				   'bathRoom'     => $data['bathroom'],
				   'description'  =>  $data['description'] ), 
			array( '%s','%s','%s','%s' ) );
		$id = $wpdb->insert_id;
						
		#success room type addition
		if($id):
			#insert room type amenities
			if(!empty($data['amenities'])):
				foreach($data['amenities'] as $amenities):
					$wpdb->insert( $wpdb->prefix.'ice_roomtypeamenities', 
					array( 'amenentiesId' => $amenities,
						   'roomTypeId'   => $id), 
					array( '%d', '%d' ) );
				endforeach;
			endif;
	
			#insert room type conditions
			/*if(!empty($data['conditions'])):
				foreach($data['conditions'] as $conditions):
					$wpdb->insert( $wpdb->prefix.'ice_roomtypeconditions', 
					array( 'conditionId' => $conditions,
						   'roomTypeId'  => $id), 
					array( '%d', '%d' ) );
				endforeach;
			endif;*/
		
			#insert room type media
			if(!empty($data['media'])):
				$wpdb->insert( $wpdb->prefix.'ice_roomtypemedia', 
					array( 'image'        => serialize($data['media']['image']), 
						   'video'        => $data['media']['video'], 
						   'roomTypeId'   => $id ), 
					array( '%s', '%s','%d' ) );
			endif;
			return $id;
		endif;
		return false;
	}
	
	#delete room type
	function delete_roomtype($id) {
		global $wpdb;
		
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtype WHERE id={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_rooms WHERE roomTypeId={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtypeamenities WHERE roomTypeId={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtypeconditions WHERE roomTypeId={$id}");
		$wpdb->query("DELETE FROM {$wpdb->prefix}ice_roomtypemedia WHERE roomTypeId={$id}");
		
		return true; 
	}
	
	#Add Room Type
	function update_roomtype($data, $id)
	{
		global $wpdb;
		if(!$id OR !$data)
			return false;
	
		#update room type
		$wpdb->update( $wpdb->prefix.'ice_roomtype', 
			array( 'roomTypeName' => $data['room_type'],
				   'bedRoom'      => $data['bed_size'],
				   'bathRoom'     => $data['bathroom'],
				   'description'  =>  $data['description'] ),
			array( 'id' => $id), 
			array( '%s','%s','%s','%s' ) );
		
		#insert room type amenities
		if(!empty($data['amenities'])):
		$delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."ice_roomtypeamenities 
				  WHERE roomTypeId = {$id};");
		if($delete):
			foreach($data['amenities'] as $amenities):
				$wpdb->insert( $wpdb->prefix.'ice_roomtypeamenities', 
				array( 'amenentiesId' => $amenities,
					   'roomTypeId'   => $id), 
				array( '%d', '%d' ) );
			endforeach;
		endif;
		endif;
	
		#insert room type conditions
		/*if(!empty($data['conditions'])):
		$delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."ice_roomtypeconditions 
				  WHERE roomTypeId = {$id};");
		if($delete):
			foreach($data['conditions'] as $conditions):
			$wpdb->insert( $wpdb->prefix.'ice_roomtypeconditions', 
				array( 'conditionId' => $conditions,
					   'roomTypeId'   => $id), 
				array( '%d', '%d' ) );
			endforeach;
		endif; 
		endif;*/
		
		#insert room type media
		if(!empty($data['media'])):
		$delete = $wpdb->query("DELETE FROM ".$wpdb->prefix."ice_roomtypemedia 
				  WHERE roomTypeId = {$id};");
			if($delete):
				$wpdb->insert( $wpdb->prefix.'ice_roomtypemedia', 
				array( 'image'        => serialize($data['media']['image']), 
					   'video'        => $data['media']['video'], 
				       'roomTypeId'   => $id ), 
				array( '%s', '%s','%d' ) );
			endif;
		endif;
		return true;
	}
	
	#Add Constants
	function add_constants($table, $data, $format) {
		global $wpdb;
		if(!$data)
			return false;
		
		$result = $wpdb->insert( $table, $data, $format );
		if($result)
			return true;
		return false;
	}
	
	#Delete Constants
	function delete_constants($table, $id) {
		global $wpdb;
		if(!$id)
			return false;
				
		$sql = "DELETE FROM {$table} WHERE id = {$id}";
		$result = $wpdb->query( $sql );
		if($result)
			return true;
		return false;
	}
	
	#Update Constants
	function update_constants($table, $data, $id,  $format) {
		global $wpdb;
		if(!$data)
			return false;
		
		$result = $wpdb->update( $table, $data, array('id'=>$id),  $format );
		if($result)
			return true;
		return false;
	}
	
}
endif;
#END