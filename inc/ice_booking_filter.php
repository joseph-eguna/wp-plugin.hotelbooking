<?php 
#Ice Booking Class Options
if(!class_exists('icebooking_class_filter')):
class icebooking_class_filter 
{
	public function notification($id = false, $errors = false, $messages = false)
	{
?>
	<div class="messages">
		<?php if($id): ?>
			<span class="success">Success</span>
		<?php endif;  ?>
		<?php if($errors):?>
			<?php foreach($errors as $kerror => $verror):?>
				<span class="error"><?php echo $verror?></span>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
<?php		
	}
	
	public function filter_form($post, $filter = array())
	{
		$error = array();
		foreach($filter['required'] as $kreq => $vreq):
			if(is_array($vreq)):
				if(empty($post[$kreq])):
					$error[] = 'Required: Empty '.ucwords(str_replace('_', ' ', $kreq));
				endif;
			else:
				if(empty($post[$vreq])):
					$error[] = 'Required: '.ucwords(str_replace('_', ' ', $vreq));
				endif; 
			endif;
		endforeach;
		
		if(!empty($error))
			return $error;
		
		return false;
	}
}
endif;
#END