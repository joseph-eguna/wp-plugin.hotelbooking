jQuery(document).ready(function($) {
 
//data convert to Object 
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
	
	//Preview Image
	$('.icon').click(function() {
		var icon_field = $(this).attr('id');
        tb_show('Upload Icon', 'media-upload.php?type=image&TB_iframe=true&post_id=0', false);
		window.send_to_editor = function(html) {
			var image_url = $('img',html).attr('src');
			$('#'+icon_field).val(image_url);
			$('#'+icon_field).before( "<img class='preview' src='"+ image_url +"' width='40px'/>" );
			tb_remove();
		}	
        return false;
    });
	
	//Select Multiple Images
	$('.select_images').click(function() {
		var parent_id = $(this).closest('form').attr('id');
        tb_show('Upload Icon', 'media-upload.php?type=image&TB_iframe=true&post_id=0', false);
		window.send_to_editor = function(html) {
			var image_url = $('img',html).attr('src');
				//image_id = $('img',html).attr('class').replace(/^\D+/g, '');
				new_image = '<span class="new_images">';
				new_image += '<img src="'+image_url+'" width="50px"/>';
				new_image += '<input type="hidden" name="image[]" value="'+image_url+'"/>';
				new_image += '<a class="remove_image"> X </a>';
				new_image += '</span>';
			
			$('#'+parent_id+ ' .gallery.images').prepend(new_image);
			
			//$('#'+icon_field).before( "<img class='preview' src='"+ image_url +"' width='40px'/>" );
			tb_remove();
		}	
        return false;
    });


	//form execute add and update constants
	$('.ice_form_submit').submit( function () {
		var jdata =  $(this).serializeObject();
		var url   =  $(this).attr('action');
		var table = $(this).closest('table').attr('id');
		var id    = $("input[name='id']").val();
		var text  = 'Added Successfully!';
		if(id){
			text  = "Updated Successfully!";
		}
		$(".messages").hide();
		$.post(ajaxurl, jdata, function(response) {
			var obj = eval('(' + response + ')');
			if(obj.error.length < 1)
			{
				$( "#"+table+" thead.add").addClass('success');
				$( "#"+table+" .message").text(text);
				setTimeout( function() { 
					$( "#"+table+" .message").text('');
					$( "#"+table+" thead.add").removeClass('success'); 
					if(id == ''){
						$("input[type='text']").val('');
						$( "#"+table+" img.preview").remove();
					}
				}, 1000);
				$( "#"+table+" tbody" ).load( "admin.php?page=add-rooms-constants #"+table+" tbody tr" );
			}
			else
			{
				html_error = '<div class="messages">';
				obj.error.forEach(function(e) {
					html_error += '<span class="error">'+e+'</span>';
				});
				html_error += '</div>';
				$( "#"+table).before(html_error);
				$( "#"+table+" img.preview").remove();
			}
				
		});
		return false;
    });
	
	/*	var attr = function (cal){
			this.position = $(cal).position({
					within:'table.calendar'
				});
			this.width  = $(cal).width();
			this.height = $(cal).width();
			this.dDate = $(cal).attr('data-date');
			
			return this;
		}
		var objx = jQuery.parseJSON( '{ "id": 1, "start": "2014-9-10", "end": "2014-9-12" }' );
		
		
		
	$("td.jtd_day").click(function() {
		
		$.each( objx, function( key, value ) {
			
			/*var dAttr = attr($("td.jtd_day[data-date='2014-9-10']"));
			var style = ''; 
			    style += 'top:'+dAttr.position.top+'px;'; 
			    style += 'left:'+dAttr.position.left+'px;'; 
			var html  = '<div class="j_booking" style="'+style+'">hello'; 
			    html += '</div>'; 
			$('.cal_content').append(html);
			//alert( key + ": " + value );	
		});
		
		//console.log(objx);
		//console.log(attr(this).dDate);
		/*$(this).position({
			within:'table.calendar'
		});
		
		
		//alert(position);
		//console.log(position);
		//console.log(Object.keys(position.top));
		/*var q = confirm("Blocked This Date?");
		if(q == true)
		{
			console.log('YES');
		}
		else
		{
			console.log('NO');
		}
	});	*/
	
	//Add Event in the calendar
	/*$('td.jtd_day').each(function(){
		$(this)
	});*/
	
	//delete data
	$('.delete').click( function () {
		var url = $(this).attr('href');
		//var del =  $(this).closest('tr');
		/*return false;
		$.get( url, url ).error( 
			function() { alert('error');}).success( 
			function() {
				del.hide();
				/*$( "#"+table+" thead.add").css({"background":"green"}).delay('1000').removeAttr('style');
				$( "#"+table+" tbody" ).load( "admin.php?page=add-rooms-constants #"+table+" tbody tr" );
			});*/
		//return false;
	});
});