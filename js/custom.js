jQuery(document).ready(function($) {
 
//data convert to Object 
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
	
	//Preview Image
	$('.icon').click(function() {
		var icon_field = $(this).attr('id');
        tb_show('Upload Icon', 'media-upload.php?type=image&TB_iframe=true&post_id=0', false);
		window.send_to_editor = function(html) {
			var image_url = $('img',html).attr('src');
			$('#'+icon_field).val(image_url);
			$('#'+icon_field).before( "<img class='preview' src='"+ image_url +"' width='40px'/>" );
			tb_remove();
		}	
        return false;
    });
	
	//Select Multiple Images
	$('.select_images').click(function() {
		var parent_id = $(this).closest('form').attr('id');
        tb_show('Upload Icon', 'media-upload.php?type=image&TB_iframe=true&post_id=0', false);
		window.send_to_editor = function(html) {
			var image_url = $('img',html).attr('src');
				//image_id = $('img',html).attr('class').replace(/^\D+/g, '');
				new_image = '<span class="new_images">';
				new_image += '<img src="'+image_url+'" width="50px"/>';
				new_image += '<input type="hidden" name="image[]" value="'+image_url+'"/>';
				new_image += '<a class="remove_image"> X </a>';
				new_image += '</span>';
			
			$('#'+parent_id+ ' .gallery.images').prepend(new_image);
			
			//$('#'+icon_field).before( "<img class='preview' src='"+ image_url +"' width='40px'/>" );
			tb_remove();
		}	
        return false;
    });


	//form execute add and update constants
	$('.ice_form_submit').submit( function () {
		var jdata =  $(this).serializeObject();
		var url   =  $(this).attr('action');
		var table = $(this).closest('table').attr('id');
		var id    = $("input[name='id']").val();
		var text  = 'Added Successfully!';
		if(id){
			text  = "Updated Successfully!";
		}
		$(".messages").hide();
		$.post(ajaxurl, jdata, function(response) {
			var obj = eval('(' + response + ')');
			if(obj.error.length < 1)
			{
				$( "#"+table+" thead.add").addClass('success');
				$( "#"+table+" .message").text(text);
				setTimeout( function() { 
					$( "#"+table+" .message").text('');
					$( "#"+table+" thead.add").removeClass('success'); 
					if(id == ''){
						$("input[type='text']").val('');
						$( "#"+table+" img.preview").remove();
					}
				}, 1000);
				$( "#"+table+" tbody" ).load( "admin.php?page=add-rooms-constants #"+table+" tbody tr" );
			}
			else
			{
				html_error = '<div class="messages">';
				obj.error.forEach(function(e) {
					html_error += '<span class="error">'+e+'</span>';
				});
				html_error += '</div>';
				$( "#"+table).before(html_error);
				$( "#"+table+" img.preview").remove();
			}
				
		});
		return false;
    });
	
	//Delete
	$('.delete').click( function () {
		var url = $(this).attr('href');
	});
	
	//Calendar
	function ej_calendar(id="calendar", date_ref=false) {
		var date = new Date();
		
		//if(date_ref != false)
		//	var date = new Date("October 13 2014");
		
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
	
		var calendar = $("#"+id).fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			selectable: true,
			selectHelper: true,
			eventLimit: true,
			select: function(start, end, allDay) {
				//var room_number = prompt('Enter room# to BLOCKED:');
					date_start = start.getFullYear()+'-'+(start.getMonth()<10?'0':'')+(parseInt(start.getMonth())+1)+'-'+(start.getDate()<10?'0':'')+start.getDate()+' 00:00:00';
					date_end   = end.getFullYear()+'-'+(end.getMonth()<10?'0':'')+(parseInt(end.getMonth())+1)+'-'+(end.getDate()<10?'0':'')+end.getDate()+' 00:00:00';
				
				/*console.log(date_start);
				console.log(date_end);
				console.log(room_number);*/
				
				pass_data_html = '';
				info = {date_start:start, date_end: end};
				$.each(info, function(ind, val){
					pass_data_html += '<input type="hidden" class="data-append" name="'+ind+'" value="'+val+'"/>';
					//console.log(val);
				});
				//console.log(date_start);
				//console.log(new Date("July 21, 1983 01:15:00"));
				dialog.dialog('open').find('form').append(pass_data_html);
				return;
			
				if (room_number) {
					var data = {
						'action': 'jaction',
						'block': 1,
						'start':date_start,
						'end':date_end,
						'room_number': room_number
					};
					$.post(ajaxurl, data, function(response) {
						if(parseInt(response) > 0){
							calendar.fullCalendar('renderEvent',
							{
								title: "Blocked room# "+room_number,
								start: start,
								end: end,
								allDay: allDay
							},
							true // make the event "stick"
							);
						}
					});
					
					
				}
				calendar.fullCalendar('unselect');
			},
			
			editable: true,
			eventClick: function(calEvent, jsEvent, view) {
				//alert('Event: ' + calEvent.title);
				//alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
				var monthNames = [ "January", "February", "March", "April", "May", "June",
								   "July", "August", "September", "October", "November", "December" ];
				date_start = monthNames[parseInt(calEvent._start.getMonth())+1] +' '+calEvent._start.getDate()+', '+calEvent._start.getFullYear();
				date_end   = monthNames[parseInt(calEvent._end.getMonth())+1] +' '+calEvent._end.getDate()+', '+calEvent._end.getFullYear();
				
				//console.log(date_start);
				//console.log(date_end);
				
				var modalhtml  = '<div class="j_modal" style="left:'+((($(window).width())/2)-150)+'px;">';
					modalhtml += '<a class="close" onclick="j_close()" href="#">X</a>';
					modalhtml += '<span class="title">'+calEvent.user.firstName+' '+calEvent.user.lastName+'</span>';
					modalhtml += '<p><em>Booking Date:</em><br>'+date_start+' to '+date_end+'</p>';
					modalhtml += '<p><em>Room #:</em> <br>     </p>';
					modalhtml += '<p><em>Email Address:</em>   '+calEvent.user.email+'</p>';
					modalhtml += '<p><em>Status:</em>          '+calEvent.status+'</p>';
					modalhtml += '</div>';
					
				$('.j_modal').remove();
				$('body').prepend(modalhtml);
			},
			events: booking_sched
		});
		
	//Dialog Box
	dialog = $( ".dialog-form" ).dialog({	
	  open: function(){
				$(this).find('form').on('submit',function(e){ e.preventDefault(); });
			},
      autoOpen: false,
      height: 'auto',
      width: 350,
      modal: true,
	  buttons: {
        "Submit": function(){ 
			var info = $(this).find('form').serializeObject();
				info.date_start = new Date(info.date_start);
				info.date_end = new Date(info.date_end);
				date_start = info.date_start.getFullYear()+'-'+(info.date_start.getMonth()<10?'0':'')+(parseInt(info.date_start.getMonth())+1)+'-'+(info.date_start.getDate()<10?'0':'')+info.date_start.getDate()+' 00:00:00';
				date_end   = info.date_end.getFullYear()+'-'+(info.date_end.getMonth()<10?'0':'')+(parseInt(info.date_end.getMonth())+1)+'-'+(info.date_end.getDate()<10?'0':'')+info.date_end.getDate()+' 00:00:00';
			
				var data = {
						'action': 'jaction',
						'purpose': info.purpose,
						'start':date_start,
						'end':date_end };
					
			if(info.purpose=='blocked'){
				data.title  = 'BLOCKED';
				data.reason = info.reason;
			}else if(info.purpose=='admin') {
				data.title    = 'Reservation by hotel admin';
				data.rooms 	  = info.select_rooms;
				data.personel = info.by_admin;
				//data.created  = info.by_admin;
			}else if(info.purpose=='customer'){
				data.title  = 'Reservation from '+info.customer_email;
				data.fname  = info.customer_fname;
				data.lname  = info.customer_lname;
				data.email  = info.customer_email;
				data.addr   = info.customer_add;
				data.rooms  = info.select_rooms;
				data.stats  = info.customer_status;
			}else{
				return false;
			}
			//console.log(data); 
			//return;		
					//start manipulation on database
					$.post(ajaxurl, data, function(response) {
						if(parseInt(response) > 0){
							calendar.fullCalendar('renderEvent',
							{
								title: data.title,
								start: data.start,
								end: data.end,
								allDay: true
							},
							true // make the event "stick"
							);
						}
					});
					//end manipulation on database;
			$(this).find('.data-append').remove();
			dialog.dialog( "close" );
		},
        Cancel: function() {
          dialog.dialog( "close" );
        }
      }
    });
	//end dialog
	
	}
	ej_calendar('calendar');
	
	//jQ dialog box select
	$('.dialog-form select[name="purpose"]').change(function(){
		var class_ref = $(this).val();
		$('.dialog-form .element').hide();
		$('.dialog-form .element.'+class_ref).css({display:'block'});
	});
	
	/*if(cal_views=='peryear')
	{
		ej_calendar('calendar0', 'January 1 2014');
		ej_calendar('calendar1', 'February 1 2014');
		ej_calendar('calendar2', 'March 1 2014');
		ej_calendar('calendar3', 'April 1 2014');
		ej_calendar('calendar4', 'May 1 2014');
	}
	console.log(cal_views);*/
});

//function j close
function j_close() {jQuery('.j_modal').remove(); }